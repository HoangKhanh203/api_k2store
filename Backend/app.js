/* eslint-disable array-callback-return */
const mongoose = require('mongoose');
const express = require('express');

const bodyParser = require('body-parser');
const ramdomString = require('randomstring');
const axios = require('axios');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

require('dotenv').config();
const PORT_DB = process.env.PORT_DB || 27011;
const DATABASE = process.env.DATABASE || 'USERS';
const URL_MONGO = process.env.URL_MONGO || 'mongodb://172.20.0.1';
const SERVICE_NAME = process.env.SERVICE_NAME || 'users-service';

const applyData = () => {
  const fs = require('fs');
  let data = fs.readFileSync('data.json');
  let student = JSON.parse(data);
  let count = 0;
  for (let index in student.data) {
    const insert = new index_module.Model_Product({
      nameProduct: Object.keys(student.data)[count],
      ItemProduct: [...student.data[index]],
    });
    insert.save((err) => {
      if (err) console.log(err);
      console.log('done.' + Object.keys(student.data)[count]);
    });
    count++;
  }
};

mongoose.connect(
  `mongodb://172.19.0.1:27017/K2STORE`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) console.log(err);
    else {
      console.log(
        `MongoDB Servers ${SERVICE_NAME} is Connect at: ${PORT_DB}`,
        err
      );
      // applyData()
    }
  }
);

const index_module = require('./models/index');
const Response = require('./response/Response');

app.post('/signin', async (req, res, next) => {
  const SignInInput = req.body;
  const value = await index_module.Model_Account.find()
    .then((items) => {
      const account = items.find((dataUser) => {
        return dataUser.user_name === SignInInput.user_name;
      });
      if (!account) {
        return res.json(Response.ErrorEmail());
      }
      if (SignInInput.password === account.password) {
        const tokenRandomString = ramdomString.generate(105);
        // update token
        /*--------------------------------------------------*/
        index_module.Model_ProfileUser.find()
          .then(async (resfindToek) => {
            await resfindToek.filter((dataProfile) => {
              if (dataProfile.token === account.token) {
                index_module.Model_Account.updateOne(
                  { token: account.token },
                  { $set: { token: tokenRandomString } }
                ).then(() => {});
                index_module.Model_ProfileUser.updateOne(
                  { token: account.token },
                  { $set: { token: tokenRandomString } }
                ).then(() => {});
              }
            });
          })
          .catch((err) => console.log('err: ', err));
        /*--------------------------------------------------*/
        return res.json(Response.SignInUpSuccess(tokenRandomString));
      }
      return res.json(Response.ErrorPassowrd());
    })
    .catch((err) => {
      console.log('err: ', err);
    });
  return value;
});

app.post('/get-info-user', async (req, res) => {
  const inputProfile = req.body;
  const value = await index_module.Model_ProfileUser.find().then((items) =>
    items.find((resdataProfile) => {
      return (
        resdataProfile.token === inputProfile.token ||
        (resdataProfile.user_name === inputProfile.user_name &&
          resdataProfile.type === inputProfile.type)
      );
    })
  );
  if (value) {
    return res.json(Response.ExistProfileUser(value));
  } else if (
    inputProfile.type === 'GOOGLE' ||
    (inputProfile.type === 'FACEBOOK' &&
      inputProfile.user_name !== undefined &&
      inputProfile.user_name !== '' &&
      inputProfile.name_user !== undefined &&
      inputProfile.name_user !== '')
  ) {
    const idCartsRandomString = ramdomString.generate(35);
    const tokenRandomString = ramdomString.generate(105);
    const createProfileUser = new index_module.Model_ProfileUser({
      type: inputProfile.type,
      id: inputProfile.id,
      token: tokenRandomString,
      name_user: inputProfile.name_user,
      user_name: inputProfile.user_name,
      carts: idCartsRandomString,
    });
    createProfileUser.save((err) => {
      if (err) console.log('err: ', err);
    });
    return res.json(Response.ExistProfileUser(inputProfile));
  }
  return res.json(Response.ErrorNotExistProfileUser());
});

app.post('/sign-up', async (req, res) => {
  const SignUpInput = req.body;
  const value = await index_module.Model_Account.find().then((items) => {
    const account = items.find((dataUser) => {
      return dataUser.user_name === SignUpInput.user_name;
    });
    if (!account) {
      const idCartsRandomString = ramdomString.generate(35);
      const tokenRandomString = ramdomString.generate(105);
      const createAccount = new index_module.Model_Account({
        token: tokenRandomString,
        user_name: SignUpInput.user_name,
        password: SignUpInput.password,
      });
      createAccount.save((err) => {
        if (err) console.log('err: ', err);
      });
      const createProfileUser = new index_module.Model_ProfileUser({
        type: 'PROFILE_USER',
        token: tokenRandomString,
        user_name: SignUpInput.user_name,
        name_user: SignUpInput.name_user,
        carts: idCartsRandomString,
      });
      createProfileUser.save((err) => {
        if (err) console.log('err: ', err);
      });
      return res.json(Response.SignInUpSuccess(tokenRandomString));
    }
    return res.json(Response.ErrorEmailExist());
  });
  return value;
});

app.post('/order-card', async (req, res) => {
  const { token, itemOrder, NgayDatHang } = req.body;
  const checklistOrderItem = await getItemId_func(itemOrder.id_item);
  let listItemOrder = [];
  for (let index in checklistOrderItem) {
    itemOrder.id_item.map((item) => {
      if (checklistOrderItem[index].id === item) {
        listItemOrder.push({
          id_item: checklistOrderItem[index].id,
          amount: itemOrder.amount[index],
        });
      }
    });
  }
  const id_carts = await index_module.Model_ProfileUser.find().then((items) => {
    const user = items.find((item) => {
      return item.token === token;
    });
    return user.carts;
  });
  const findCarts = await index_module.Model_Carts.find().then((items) =>
    items.find((item) => {
      return item.id_carts === id_carts;
    })
  );
  const orderItem = [];
  listItemOrder.map((item) => {
    orderItem.push({
      item: item.id_item,
      amount: item.amount,
    });
  });
  if (!findCarts) {
    const insertCart = new index_module.Model_Carts({
      id_carts: id_carts,
      listOrder: [
        {
          date: NgayDatHang,
          orderItem: orderItem,
        },
      ],
    });
    insertCart.save((err) => {
      if (err) console.log('err: ', err);
      console.log('order sucessful');
    });
  } else {
    index_module.Model_Carts.findOneAndUpdate(
      { id_carts: id_carts },
      {
        $push: {
          listOrder: {
            date: NgayDatHang,
            orderItem: orderItem,
          },
        },
      }
    ).then(() => {});
  }
});

app.post('/search-items', async (req, res) => {
  const valueSeach = req.body;
  const value = await getItemName_func(valueSeach);
  if (value.length === 0) {
    return res.json(dataProductItem_Errors());
  }
  return res.json(Response.dataProductItem_Success(value));
});

const deduplicate = (arr) => {
  let isExist = (arr, x) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id === x.id) return true;
    }
    return false;
  };
  let ans = [];
  arr.forEach((element) => {
    if (!isExist(ans, element)) ans.push(element);
  });
  return ans;
};

const getItemId_func = async (id_item) => {
  const value = await index_module.Model_Product.find().then((res) => {
    let arrItem = [];
    res.find((item) => {
      for (let index in item.ItemProduct) {
        id_item.map((res_id_item) => {
          if (item.ItemProduct[index].id === res_id_item) {
            arrItem.push(item.ItemProduct[index]);
          }
        });
      }
    });
    return deduplicate(arrItem);
  });
  return value;
};
const getItemName_func = async (valueSearch) => {
  const value = await index_module.Model_Product.find().then((res) => {
    let arrItem = [];
    res.find((item) => {
      for (let index in item.ItemProduct) {
        let search_item = new RegExp(`\\b${valueSearch.toUpperCase()}`).exec(
          item.ItemProduct[index].title.toUpperCase()
        );
        let search_type = new RegExp(`\\b${valueSearch.toUpperCase()}`).exec(
          item.ItemProduct[index].type.toUpperCase()
        );
        if (search_item !== null || search_type !== null) {
          arrItem.push(item.ItemProduct[index]);
        }
      }
    });
    return deduplicate(arrItem);
  });
  return value;
};
app.get('/get-data-product', async (req, res) => {
  const { nameProduct } = req.body;
  const value = await index_module.Model_Product.find()
    .then((items) =>
      items.find((item) => {
        if (item.nameProduct === nameProduct) {
          return item.ItemProduct;
        }
      })
    )
    .catch((err) => {
      console.log('err: ', err);
    });
  return res.json(Response.dataProductItem_Success(value.ItemProduct));
});

app.get('/events', async (req, res) => {
  index_module.Model_Event.find()
    .then((res) => res.map((item) => ({ ...item._doc })))
    .catch((err) => {
      console.log('err: ', err);
    });
});
app.get('/get-item-by-id', async (req, res) => {
  const { id_item } = req.body;
  let value;
  if (id_item !== undefined) {
    value = await getItemId_func(id_item);
  }
  return res.json(Response.dataProductItem_Success(value));
});

app.listen({ port }, async () => {
  console.log(`Apollo Server on http://localhost:${port}`);
});
