const mongoose = require('mongoose');

const { Schema } = mongoose;

const AccountUsers = new Schema({
  token: {
    type: String,
    require: true,
  },
  user_name: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true,
  },
});
module.exports = mongoose.model('USERS', AccountUsers, 'USERS');
