const Model_Event = require('./event.model');
const Model_Carts_Order = require('./carts_order.model');
const Model_Product = require('./product.model');
const Model_Account = require('./account.model');
const Model_ProfileUser = require('./profileUser.model');
const Model_Carts = require('./carts.model');
const Model_Flash_Sale = require('./flash_Sale.model');
module.exports = {
  Model_Event,
  Model_Product,
  Model_Account,
  Model_ProfileUser,
  Model_Carts_Order,
  Model_Carts,
  Model_Flash_Sale
};
