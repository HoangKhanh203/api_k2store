const mongoose = require('mongoose');
const { Schema } = mongoose;

const productsSchema = new Schema({
  cookie: {
    type: String,
    require: true
  },
  list_order:{
    type: Array,
  }
});
module.exports = mongoose.model('CARTS_ORDER', productsSchema, 'CARTS_ORDER');
