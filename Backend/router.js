const express = require("express");
const Router = express.Router();

const Controllers = require('HandleAPI/Controllers');
const Middleware = require("HandleAPI/Middleware");
const routerAPI = require('./router_api');

Router.post(`/${routerAPI.POSTS.CREATE_POST.URL}`, Controllers.HandlePost.createPost)
Router.post(`/${routerAPI.POSTS.EDIT_POST.URL}`, Middleware.checkPost, Controllers.HandlePost.editPost);
Router.post(`/${routerAPI.POSTS.DELETE_POST.URL}`, Middleware.checkPost, Controllers.HandlePost.deletePost);

Router.post(`/${routerAPI.POSTS.LIKE_POST.URL}`, Middleware.checkPost, Controllers.HandleInteractivePost.likePost)
Router.post(`/${routerAPI.POSTS.COMMENT_POST.URL}`, Middleware.checkPost, Controllers.HandleInteractivePost.commentPost)
Router.post(`/${routerAPI.POSTS.SHARE_POST.URL}`, Middleware.checkPost, Controllers.HandleInteractivePost.sharePost)
Router.post(`/${routerAPI.POSTS.CHANGE_AVATAR_THEME.URL}`, Controllers.HandlePost.changeAlbumAvatarTheme)


Router.get(`/${routerAPI.POSTS.GET_LIST_POST_FOR_HOME_PAGE.URL}`,Middleware.checkPost, Controllers.HandlePost.getListPostHomePage)
Router.get(`/${routerAPI.POSTS.GET_INFO_POST.URL}`, Middleware.checkPost, Controllers.HandlePost.getPostByID);
module.exports = Router;