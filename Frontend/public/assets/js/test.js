
let dataSearch ;
let total = 0 ;
let value_search = '';
let CurrentPage=1;
const search_Page =async () =>{
        const inputsearch = document.getElementById("input-search").value
        value_search = getParameterByName('value_search');
        value_search = (value_search).toString().trim();
        const value = await search_item(inputsearch? inputsearch:value_search);
        if(value){
                const { list_item, total_item } = value;
                dataSearch = list_item;
                total = total_item;
                sliceData();
                Pagination();
        }else{
            document.getElementsByClassName("Header-Search")[0].setAttribute("class","Header-Search");
            showNotification.error('Không Tồn Tại Sản Phẩm')            
        }
        document.getElementsByClassName("Header-Search")[0].setAttribute("class","Header-Search");

}
const showDataSearch = (paginValue) =>{
    const sortData = increase(paginValue);
    const data = elementItem.search_page(sortData);
    let html = "";
    for (let index in data) {
      html += data[index];
    }
    document.getElementById("body-search-page").innerHTML = html;
    cart.cartListenerOrder();
    document.getElementById('txtValueSearch').innerText = value_search;
    document.getElementById('total-item').innerText = total;
}


const Pagination = () =>{
    const prenext = document.querySelectorAll('#PreNext');
    for(let click of prenext){
        click.addEventListener('click',(e)=>{   
            switch(e.target.dataset.value){
                case 'previous':
                    if(CurrentPage > 1 )
                       CurrentPage--;
                    sliceData()
                break;
                case 'next':
                    CurrentPage++;
                    sliceData()
                break;
            }
        });
    }
    const numberPagin = document.getElementById('number-pagination').getElementsByTagName('li');
    console.log(numberPagin)
    for(let click of numberPagin){
        click.addEventListener('click',async (e)=>{ 
            CurrentPage++;
            console.log('conumber',CurrentPage);
            sliceData();
            return;
        });
    }
    return;
}

const sliceData = () =>{
    const begin = (CurrentPage-1)*13;
    const end = ((CurrentPage)* 13)-1;
    let paginValue = dataSearch.slice(begin,end);
    showDataSearch(paginValue);
    PaginationNumber();
}
const PaginationNumber = () =>{
    const lengthData = Math.ceil(dataSearch.length/12);
    const lengthPagin = lengthData <= CurrentPage+6 ?lengthData : CurrentPage+6;
    const numberLi = lengthData <= CurrentPage+6 ? 2 : CurrentPage+1;
    let htmlPagin =``;
    for (let index = numberLi; index <  lengthPagin; index++) {
        htmlPagin += `<li data-value="${index}">${index}</li>`; 
    }
    if(lengthData > 7){
        htmlPagin +="<li>...</li>";
        htmlPagin +=`<li data-value="${dataSearch.length}">${lengthData}</li>`;
    }
    document.getElementById('number-pagination').innerHTML = htmlPagin;
}
const input_search = async (e) =>{
    if(e.value.length > 2){
        const value =await search_item(e.value);
        let lengthItem = 0;
        let html = "";
        if(value.status ==='OK'){
            const { total_item, list_item } = value;
            let someItem = [];
            lengthItem =total_item;
            const length = total_item > 4 ? 4 : total_item
            for(let index = 0 ; index < length  ; index++){
                someItem.push(list_item[index])
            }
            const data = elementItem.SearchItem(someItem);
            for (let index in data) {
                html += data[index];
            }
            const href_showfull = document.getElementById('showFull');
            href_showfull.href = './search.html?value_search='+e.value;
        }else{
            html = 'Sản Phẩm Hiện Chưa Có'
        }
        document.getElementById('total_item').innerText = lengthItem;
        document.getElementById('list-content-search').innerHTML = html;
        document.getElementsByClassName("Header-Search")[0].className += " active-list-search";
    }else{
        document.getElementsByClassName("Header-Search")[0].setAttribute("class","Header-Search");
    }
}
const search_item = async (value_search) =>{
    if(value_search.length > 2){
        const value =  await CallAPI.searchItem(value_search);
        const { searchItem } = await value;
        const {total_item,list_item } = searchItem
        if(total_item > 0){
            dataSearch = list_item;
        }
        return searchItem;
    }
}
const sortItem = () => {
    const select = document.getElementById("sortItem");
    const option = select.options[select.selectedIndex].value;
    const begin = (CurrentPage)*13;
    const end = ((CurrentPage+1)* 13)-1;
    let value;
    switch(option){
        case 'i_p':
            value = increase(dataSearch);
        break;
        case 'd_p':
            value = decrease(dataSearch);
        break;
        default:
            value = increase(dataSearch);
        break;
    }
    const paginValue = value.slice(begin,end);
    showDataSearch(paginValue)
    cart.cartListenerOrder();
}
const increase = (dataSearch) => {
    for(let index = 0 ;index < dataSearch.length -1;index++){
        for(let y =index+1; y < dataSearch.length ;y++){
            if(parseInt(dataSearch[index].price) > parseInt(dataSearch[y].price)){
                const resualt = dataSearch[index];
                dataSearch[index] = dataSearch[y];
                dataSearch[y] = resualt;
            }
        }
    };
    return dataSearch;
}
const decrease = (dataSearch) => {
    for(let index = 0 ;index < dataSearch.length -1;index++){
        for(let y =index+1; y < dataSearch.length ;y++){
            if(parseInt(dataSearch[index].price) < parseInt(dataSearch[y].price)){
                const resualt = dataSearch[index];
                dataSearch[index] = dataSearch[y];
                dataSearch[y] = resualt;
            }
        }
    };
    return dataSearch;
}