let queryProducts = `query{
    products{
      name
      image
      description
    }
  }`;
// let mutationLogin = `mutation{
//   signIn(input: {user_name:"hoangkhanh2032000@gmail.com",password:"123456",remember_me: true}){
//     status
//     message
//     errors{
//       error_code
//       message
//     }
//     auth_token
//   }
//   }`;
// phai co chu query: ``
var CallAPI = {
  login: (signInInput) => {
    const mutationLogin = `mutation(
        $user_name: String!
        $password: String!
        $remember_me: Boolean
      ) {
        signIn(
          input: {
            user_name: $user_name
            password: $password
            remember_me: $remember_me
          }
        ) {
          status
          errors {
            error_code
            message
          }
          auth_token 
        }
      }`;
    const variablesSignIn = {
      user_name: signInInput.user_name,
      password: signInInput.password,
      remember_me: signInInput.remember_me,
    };
    return API(mutationLogin, variablesSignIn);
  },
  getProfile: (inputProfile) => {
    const mutaionGetProfile = `
     mutation(
              $type: Profile_Type!
              $token: String
              $id: String
              $user_name: String
              $name_user: String
              ) {
      getInforUser(input:{
                          type: $type
                          token: $token
                          id: $id
                          user_name: $user_name
                          name_user:  $name_user    
      }){
        status
        message
        errors{
          error_code
          message
        }
        student{
          name_user
          token
        }
      }
    }
      `;
    const variablesGetProfile = {
      type: inputProfile.type,
      token: inputProfile.token,
      id: inputProfile.id,
      user_name: inputProfile.user_name,
      name_user: inputProfile.name_user,
    };
    return API(mutaionGetProfile, variablesGetProfile);
  },
  signUp: (signUpInput) => {
    const mutationSignUp = `
    mutation (
      $user_name: String!
      $password: String!
      $name_user: String!
    ){
      signUp(input:{user_name: $user_name,password: $password,name_user: $name_user}){
            status
        message
        errors{
          error_code
          message
        }
        auth_token
      }
    }
    `;
    const variablesSignUp = {
      user_name: signUpInput.user_name_up,
      password: signUpInput.password_up,
      name_user: signUpInput.your_name_up,
    };
    return API(mutationSignUp, variablesSignUp);
  },
  getItemProduct: (nameProduct) => {
    const mutationGetItemTopSale = `
    query($nameProduct: String!){
      getDataProduct(input:{nameProduct:$nameProduct}){
        status
        message
        errors{
          error_code
          message
        }
        total_item
        list_item{
          id
          title
          price
          image
          imageSec
          amount
          forphone
          type
          ranking
          trademark
        }
      }
    }
    `;
    const variablesgetItemProduct = {
      nameProduct,
    };
    return API(mutationGetItemTopSale, variablesgetItemProduct);
  },
  getItemID: (id_item) => {
    const mutationGetItemID = `
    query($id_item: [String!]){
      getItemId(input:{id_item:$id_item}){
        status
        message
        errors{
          error_code
          message
        }
        list_item{
            id
            title
            price
            image
            imageSec
            amount
            forphone
            type
            ranking
            trademark
        }
    }
    }
    `;
    const variablesgetItemID = {
      id_item,
    };
    return API(mutationGetItemID, variablesgetItemID);
  },
  searchItem: (valueSearch) => {
    const mutationSearch = `
    mutation($valueSearch:String!){
      searchItem(input: $valueSearch){
        status
        message
        errors{
          error_code
          message
        }
        total_item
        list_item{
              id
              title
              price
              image
              imageSec
              amount
              forphone
              type
              ranking
        }
      }
    }
    `;
    const variablesValueSearch = {
      valueSearch,
    };
    return API(mutationSearch, variablesValueSearch);
  },
};
async function API(queryType, variablesInput) {
  let data_CallAPI = await fetch("https://k2store-be.herokuapp.com/graphql/", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: queryType,
      variables: {
        ...variablesInput,
      },
    }),
  })
    .then((res) => res.json())
    .then((res) => {
      return res.data;
    })
    .catch((err) => console.log(err));
  return data_CallAPI;
}
