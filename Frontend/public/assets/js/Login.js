var authentication;
// var autoLogin = false;
// let Layout;
// let Modal;
// let close;
// // let back = document.getElementById("back");
// let clicks;
// //--------------------LOGIN----------------------
// const formLogin = () => {
//   const TypeModal = [
//     {
//       LayoutModal: "Layout_Login",
//       NameClick: "Login",
//     },
//     {
//       LayoutModal: "Layout_Register",
//       NameClick: "Register",
//     },
//     {
//       LayoutModal: "Layout_Localtion",
//       NameClick: "Location",
//     },
//   ];
//    Modal = document.getElementById("ModalForm");
//    close = document.getElementById("close");
//   // let back = document.getElementById("back");
//    clicks = document.querySelectorAll("#Click_Popup");
  
//   for (let click of clicks) {
//     click.addEventListener("click", function (e) {
//       document.getElementById("close").style.left = "87%";
//       if (e.target.innerText === "LogOut") {
//         document.getElementById("txtLogin").style.display = "inline-block";
//         document.getElementById("txtLogout").style.display = "none";
//         document.getElementById("hiUser").innerText = "Welcome To K2Store";
//         authentication = false;
//         localStorage.removeItem('__token__');
//       }
//       if (e.target.innerText === "Location")
//         document.getElementById("close").style.left = "120%";
//       Modal.style.display = "none";
//       for (let key in TypeModal) {
//         Layout = document.getElementById(TypeModal[key].LayoutModal);
//         Layout.style.display = "none";
//       }
//       for (let key in TypeModal) {
//         Layout = document.getElementById(TypeModal[key].LayoutModal);
//         if (TypeModal[key].NameClick == e.target.innerText) {
//           if (Modal.style.display == "block") {
//             Modal.style.display = "none";
//             Layout.style.display = "none";
//           } else {
//             Layout.style.display = "block";
//             Modal.style.display = "block";
//             close.style.display = "block";
//             // if (e.target.innerText == "Register") {
//             //   document.getElementById("back").style.display = "Block";
//             // }
//             break;
//           }
//         }
//       }
//     });
//   }
//   // back.addEventListener("click", function () {
//   //   back.style.display = "none";
//   //   Layout.style.display = "none";
//   //   document.getElementById("Layout_Login").style.display = "block";
//   // });
//   close.addEventListener("click", function () {
//     Modal.style.display = "none";
//     Layout.style.display = "none";
//   });
// }
// goij thang validator from ./validator.js
const eye = document.querySelectorAll('#showSignIn');
const passwordShowHide = document.querySelectorAll('.inputPassword');

// const passwordShowHide = getP.item('0').children[2];
for(let click in eye){
  eye[click].addEventListener('click',(e) =>{
    const click2 = parseInt(e.target.dataset.eye);
    const Showhide = parseInt(e.target.dataset.input);
    if(click2 == 1){
      passwordShowHide.item(Showhide).children[2].setAttribute('type','text')
      }else{
      passwordShowHide.item(Showhide).children[2].setAttribute('type','password')
      }

    let show = click % 2 == 0 ? parseInt(click)+1 :  parseInt(click) -1;
    eye[parseInt(click)].style.display = 'none';
    eye[show].style.display = 'block';

  })
}



async function btnLogin() {
  document.getElementById("MessageErrorAccount").style.display = "none";

  let value = document.querySelectorAll(".SignIn");
  const valueInputSignIn = {
    user_name: value[0].value,
    password: value[1].value,
  };
  const emptyArr = Object.values(valueInputSignIn);
  console.log(emptyArr)
  const validate = {
    empty: validator.validateEmpty(emptyArr),
    email: validator.validateEmail(valueInputSignIn.user_name),
  };
  const delay = await setTimeout(() => {
    if (validate.empty != null) {
      clearTimeout(delay);
      return showMessErr(validate.empty);
    } else if (validate.email != null) {
      clearTimeout(delay);

      return showMessErr(validate.email);
    } else {
      clearTimeout(delay);
      return signin(valueInputSignIn);
    }
  }, 500);
}

/*---------------------------------------------- REGISTER------------------------------------------------------*/
async function btnRegister() {
  document.getElementById("MessageErrorAccount").style.display = "none";
  document.getElementById('regulationPassword').style.display="none"

  let value = document.querySelectorAll(".SignUp");
  console.log(value)
  const valueInputSignUp = {
    user_name_up: value[0].value,
    password_up: value[1].value,
    cofirmpassword_up: value[2].value,
    your_name_up: value[3].value,
    // your_phone_up: value[4].value,
  };
  const emptyArr = Object.values(valueInputSignUp);
  const validate = {
    empty: validator.validateEmpty(emptyArr),
    email: validator.validateEmail(valueInputSignUp.user_name_up),
    lengthName: validator.validateLengthString(valueInputSignUp.your_name_up),
    password: validator.validatePassword(
      valueInputSignUp.password_up,
      valueInputSignUp.cofirmpassword_up
    ),
  };
  const delay = await setTimeout(() => {
    if (validate.empty != null) {
      clearTimeout(delay);

      return showMessErr(validate.empty);
    } else if (validate.email != null) {
      clearTimeout(delay);

      return showMessErr(validate.email);
    } else if (validate.password != null) {
      clearTimeout(delay);
      document.getElementById('regulationPassword').style.display="block"
      return showMessErr(validate.password.message);
    } else if (validate.lengthName != null) {
      clearTimeout(delay);

      return showMessErr(validate.lengthName);
    } else {
      clearTimeout(delay);

      return Register(valueInputSignUp);
    }
  }, 500);
}

/*-------------------------------FUNCTION-------------------------------- */

async function Register (valueInputSignUp) {
  const dataSignUp = await CallAPI.signUp({
    ...valueInputSignUp,
  });
  if (dataSignUp) {
    const { signUp } = dataSignUp;
    if (signUp.errors != null) {
      showNotification.error(signUp.errors[0].message)
      showMessErr(signUp.errors[0].message);
    } else {
      const { auth_token } = signUp;
      showNotification.success('Đăng Kí Thành Công')
      localStorage.setItem("__token__", auth_token);
      // localhost:9080
      window.location.href = 'http://127.0.0.1:5502/Frontend/public/HTML/index.html';      
    }
  }
  return dataSignUp;
};
async function signin(valueInputSignIn){

  const dataSignin = await CallAPI.login({
    ...valueInputSignIn,
  });
  if (dataSignin) {
    const { signIn } = dataSignin;
    if (signIn.errors != null) {
      authentication = false;
      showMessErr(signIn.errors[0].message);
      showNotification.error('Đăng Nhập Thất Bại \n '+signIn.errors[0].message)
    } else {
      const { auth_token } = signIn;
      localStorage.setItem("__token__", auth_token);
      headers();
      window.location.replace('./index.html'); 
      showNotification.success('Đăng Nhập Thành Công ');
    }
  }
  return dataSignin;
};

function showMessErr(mess) {
  document.getElementById("MessageErrorAccount").style.display ="block";
  document.getElementById("MessageErrorAccount").innerText = mess;
}

function showColorScorePass() {
  document.getElementById('colorStrongPassword').style.display="block";
  document.getElementById('margin-down').style.marginTop="15px";
  const render = document.getElementById("colorStrongPassword");
  const itemMain = {
    color: "",
    percent: 0,
  };
  const currentWidth = parseInt(render.style.width.split("%"));
  const password = document.querySelectorAll(".SignUp")[1].value;
  checkSymbolPassword(password, (item) => {
    (itemMain.color = item.color), (itemMain.percent = item.percent);
  });
  if (currentWidth != itemMain.percent) renderColor(itemMain);
}

function renderColor(itemMain) {
  const render = document.getElementById("colorStrongPassword");
  render.style.background = itemMain.color;
  const currentWidth = parseInt(render.style.width.split("%"))
    ? parseInt(render.style.width.split("%"))
    : 0;
  let percent = currentWidth;
  let index = 1;
  if (percent >= 0) {
    if (currentWidth > itemMain.percent) {
      index = -1;
    }
    const renderLayout = setInterval(() => {
      if (percent === itemMain.percent) {
        clearInterval(renderLayout);
      }
      render.style.width = percent + "%";
      percent = percent + index;
    }, 10);
  }
}

// google
function onSuccess(googleUser) {
  gapi.client.load('oauth2', 'v2', function () {
      var request = gapi.client.oauth2.userinfo.get({
          'userId': 'me'
      });
      request.execute(async function (resp) {
         icon_login_logout({
              type: 'GOOGLE',
              id: resp.id,
              user_name: resp.email,
              name_user: resp.name
          });
      });
  });
}
// function signOut() {
//   console.log('co')
//   var auth2 = gapi.auth2.getAuthInstance();
//   localStorage.removeItem('__token__');
//   auth2.disconnect();
// }
function onFailure(error) {
  alert(error);
}

function signOut() {
  fbLogout();
    showNotification.success('Logout Thành Công')
    document.getElementById("txtLogin").style.display = "inline-block";
    document.getElementById("txtLogout").style.display = "none";
    document.getElementById("hiUser").innerText = "Welcome To K2Store";
    authentication = false;
    localStorage.removeItem('__token__');
}