
Number.prototype.format = function(n, x) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
  return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
};
var elementItem = {
  TopSale: (list_item) => {
    return list_item.map((item) => {
        return `
            <div class="itemTopSale">
              <div class="contentItemTopSale">
                <a href="detail.html?id_item=${item.id}">
                      <div class="col-6">
                        <img id="imgItem"src="${item.image}" alt="">
                      </div>
                </a>
                <div class="col-6">
                    <div class="inforItemTopSale">
                      <a href="detail.html?id_item=${item.id}">
                          <p id="titleItem" >${item.title}</p>
                          <p> ${starItem(item.ranking)}</p>
                          <p id="price" data-price="${item.price}">${parseInt(item.price).Format_Currency()} VNĐ</p>
                      </a>
                        <button data-idItem="${item.id}" id="btnBuy">Mua Hang</button>
                    </div>
                </div>
              </div>
            </div>
        `;
    });
  },
  cartIconOder: (list_order) =>{
    return list_order.map((item)=>{
        return  `
                <a class="item-cart" href="detail.html?id_item=${item.id_item}">
                    <img src="${item.image}" alt="">
                    <p id="name-item-cart">${item.title}</p>
                    <p id="txtgiatien">Giá Tiền: <span id="price-item-cart">${parseInt(item.price).Format_Currency()} VNĐ</span></p>
                    <p>Số Lượng: <span id="price-item-cart">${item.amount}</span></p>
                </a>
        `
    })
  },
  FlashSale: (list_item) => {
    return list_item.map(item =>{
      const percentFS = Math.floor(Math.random() * 7) + 1;
      return `
        <div id="item-type-2">
            <div class="content-type-2">
              <div class="ribbon ribbon-top-left"><span>${percentFS*10}%</span></div>
                <a href="detail.html?id_item=${item.id}">
                  <img id="imgItem" src="${item.image}"alt="">
                  <p id="titleItem">${item.title}</p>
                  <p>
                    ${starItem(item.ranking)}
                  </p>
                  <p id="price" data-price="${item.price}" ><span id="price_old">${parseInt(item.price*percentFS).Format_Currency()} VNĐ</span><span id="price_new">${parseInt(item.price).Format_Currency()} VND</span></p>
                </a>
                <button  data-idItem="${item.id}" id="btnBuy">Mua Hang</button>
            </div>
        </div>
      `
    })
  },
  detailItem: (list_item) =>{
    const infor_item =[];
    let value=''; 
    list_item.map((item) => {
      value = `
        <div id="img-sec-item">
            ${load_img_sec(item.imageSec)}
        </div>
      `;
      infor_item.push(value);
      value = `
                <p id="titleItem">${item.title}</p>
                <p id="ranking">${starItem(item.ranking)}</p>
                <p id="trademark">Thương Hiệu: <span id="branch">${item.trademark}</span></p>
                <p>Mã Sản Phẩm: <span id="id_item">${item.id}</span></p>
                <hr>
                <p id="price" data-price="${item.price}">${parseInt(item.price).Format_Currency()} VND</p>
                <p>Dành Cho Điện Thoại: <span id="forPhone">${item.forphone}</span></p>
                <p>Loại Sản Phẩm: <span id="type">${item.type}</span></p>
      
      `;
      infor_item.push(value);
    
    });
    return infor_item;
  },
  SearchItem: (list_item) =>{
    return list_item.map(item =>{
      return `
        <a href="./detail.html?id_item=${item.id}">
          <div id="item-search">
              <ul id="image-item-search">
                  <img src="${item.image}" alt="" />
              </ul>
              <ul id="title-item-search">
                  <li id="title">${item.title}</li>
                  <li id="price">${parseInt(item.price).Format_Currency()} VND</li>
              </ul>
          </div>
        </a>
      `
    })
  },
  search_page: (list_item) => {
    return list_item.map(item =>{
      
      const price = item.price === null ? 0 : parseInt(item.price).Format_Currency()
      const percentFS = Math.floor(Math.random() * 7) + 1;
      return `
        <div id="item-type-2">
            <div class="content-type-2">
              <div class="ribbon ribbon-top-left"><span>${percentFS*10}%</span></div>
                <a href="detail.html?id_item=${item.id}">
                  <img id="imgItem" src="${item.image}"alt="">
                  <p id="titleItem">${item.title}</p>
                  <p>
                    ${starItem(item.ranking)}
                  </p>
                  <p id="price" data-price="${item.price}" ><span id="price_old">${(item.price*percentFS).Format_Currency()} VNĐ</span><span id="price_new">${price} VND</span></p>
                </a>
                <button  data-idItem="${item.id}"  id="btnBuy">Mua Hang</button>
            </div>
        </div>
      `
    })
  },
  cart_order_page: (list_item) =>{
    return list_item.map(item =>{
    const price = parseInt(item.price).Format_Currency() === null ? 0 : parseInt(item.price).Format_Currency();
      return `
      <div id="item-cart-order">
                    <div id="img-item"><img src="${item.image}" alt=""></div>
                    <div id="content-item-cart">
                        <div id="content-left">
                            <p id="title">${item.title}</p>
                            <p>Nhãn Hiệu: <span id="trademark">K2Store</span></p>
                            <button class="btn-design color-11" onclick="DeleteItem(this)" data-id_item="${item.id_item}">Xóa</button>
                        </div>
                        <div id="content-right">
                            <p>Giá Tiền: <span id="price">${price} VND</span></p>
                            <p id="chooseAmount">
                                <span data-value="down" id="downup" data-id_item="${item.id_item}">-</span>
                                <input type="text" id="amount" value=${item.amount}>
                                <span data-value="up" id="downup" data-id_item="${item.id_item}">+</span>
                            </p>
                        </div>
                    </div>
                </div>
      
      `
    })
  }
};
const starItem = (value) => {
  let start = '<span class="fa fa-star ';
  let end = '"></span>';
  let starResualt = "";
  for (let index = 0; index < 5; index++) {
    if (index < value) starResualt += start + "checked" + end;
    else starResualt += start + end;
  }
  return starResualt;
};
const load_img_sec = (value) =>{
  let html_img=''
   for(let index in value){
    html_img+= `<img data-imgkey="${index}" src="${value[index]}" alt="">`
  };
  return html_img;
}
