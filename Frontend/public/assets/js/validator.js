const ErorrMessage = {
  EMPTYVALUE: {
    message: "Vui Lòng Nhập Đầy Đủ!!!!!!",
  },
  FULLNAME: {
    SMALLER_2_CHARATER: {
        message: "Full Name must be at least 2 charaters!!!!!!",
    },
    BIGGER_50_CHARATER: {
      message: "Full Name must be Smaller 50 charaters!!!!!!",
    },
    NAME: {
      message: "Đây Không Phải Là Tên Của Bạn"
    }
  },
  EMAIL_WRONG: {
    message: "Đây Không Phải Là Email !!!!!!",
  },
  PHONE_WRONG: {
    message: "This not phone numbers!!!!!! \n 11 numbers",
  },
  PASSWORD_STRONG: {
    LENGTH: {
      error_code: "ERS_CODE_001",
      message: "Mật Khẩu Không Hợp Lệ ",
    },
    MATCH: {
      error_code: "ERS_CODE_002",
      message: "Mật Khẩu Không Khớp Với Nhau",
    },
  },
};

var validator = {
  validateEmpty: (Value) => {
    const emptyAll = Value.filter((res) => {
      return res == "";
    });
    console.log(Value)
    console.log(emptyAll)
    if (emptyAll.length > 1 ) {
      return ErorrMessage.EMPTYVALUE.message;
    }
    return null;
  },
  //----------------------------------------------------------

  validateEmail: (Value) => {
    if (!RGX.email.test(Value)) {
      return ErorrMessage.EMAIL_WRONG.message;
    }
    return null;
  },

  //---------------------------------------------------------

  validateLengthString: (Value) => {
    if(!RGX.yourname.test(Value)) return ErorrMessage.FULLNAME.NAME.message;
    if(RGX.phone.test(Value)) return ErorrMessage.FULLNAME.NAME.message;
    if (Value.length <= 2) return ErorrMessage.FULLNAME.SMALLER_2_CHARATER.message;
    if (Value.length >= 50) return ErorrMessage.FULLNAME.BIGGER_50_CHARATER.message;
    return null;
  },

  //---------------------------------------------------------

  validatePhoneNumbers: (Value) => {
    if (!RGX.phone.test(Value)) {
      return ErorrMessage.PHONE_WRONG.message;
    }
    return null;
  },
  validatePassword: (password, confirmPassword) => {
    if (!RGX.password.test(password)) {
      return ErorrMessage.PASSWORD_STRONG.LENGTH;
    } else if (password != confirmPassword) {
      return ErorrMessage.PASSWORD_STRONG.MATCH;
    }
    return null;
  },
};
