var dataStore = {
  TopSale: async () => {
    const value = await CallAPI.getItemProduct("TOPSALE");
    if (value !== null) {
      const { getDataProduct } = value;
      if (getDataProduct.status === "OK") {
        const dataTopSale = elementItem.TopSale(getDataProduct.list_item);
        let html = "";
        for (let index in dataTopSale) {
          html += dataTopSale[index];
        }
        loadDataPage = true;
        document.getElementById("listItemTopSale").innerHTML = html;
      }
    }
  },
  FlashSale: async () => {
    loadDataPage = false
    const value = await CallAPI.getItemProduct("PHUKIENKHAC");
    if (value !== null) {
      const { getDataProduct } = value;
      if (getDataProduct.status === "OK") {
        const data = elementItem.FlashSale(getDataProduct.list_item);
        let html = "";
        for (let index in data) {
          html += data[index];
        }
        document.getElementById("list-item-flash-sale").innerHTML = html;
        loadDataPage = true;
        autoChangeImage();
      }
    }
  },
  cartIconOder: () =>{
    const value = sessionStorage.getItem('__CARTS__') ? JSON.parse(sessionStorage.getItem('__CARTS__')) : [];
    if(value.length > 0){
      const dataCart = elementItem.cartIconOder(value)
      let html ='';
      const length =  dataCart.length >= 4 ? 4: dataCart.length
      for (let index = 0; index < length; index++) {
        html += dataCart[index];
        
      }
    document.getElementById("list-order-cart").innerHTML = html;
    const amountCart = document.querySelectorAll('#amountItemCart');
    amountCart[0].innerText = dataCart.length;
    amountCart[1].innerText = dataCart.length;
    }
  }
};
