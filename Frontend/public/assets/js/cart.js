var cart  = {
    cartListenerOrder: () =>{
        const cart = document.querySelectorAll('#btnBuy');
        for (let index = 0; index < cart.length; index++) {
            cart[index].addEventListener('click',e=>{
                const amount = document.getElementById('amount');
                const imgCart = document.querySelectorAll('#imgItem');
                const title = document.querySelectorAll('#titleItem');

                const price = document.querySelectorAll('#price');
                let cartOrder = sessionStorage.getItem('__CARTS__') ? JSON.parse(sessionStorage.getItem('__CARTS__')) : [] ;
                const ItemCart = {
                    id_item: cart[index].getAttribute('data-idItem'),
                    title: title[index].textContent,
                    price: price[index].getAttribute('data-price'),
                    image: imgCart[index].getAttribute('src'),
                    amount: !amount ? 1 : parseInt(amount.value)
                }
                let check = false;
                for(let index in cartOrder){
                    if(ItemCart.id_item === cartOrder[index].id_item){
                        cartOrder[index].amount +=1;
                        check  = true;
                    }
                }
                if(!check){
                    cartOrder.push(ItemCart)
                }
                sessionStorage.setItem('__CARTS__',JSON.stringify(cartOrder));
                showNotification.success('Mua Hang Thanh Cong')
                dataStore.cartIconOder();
            })
        }
    },
}
let total_money=0;
let coupon_code = 0;
const pay = () =>{
    if(sessionStorage.getItem('__CARTS__')){
        sessionStorage.removeItem('__CARTS__');
        showNotification.success("Đặt Hàng Thành Công");
        showNotification.success("Cảm Mơn "+name_user+" đã Mua Hàng \n Tổng Tiền là: "+total_money + " VNĐ");
        total_money = 0;
        coupon_code = 0;
        Cart_Order_Page();
    }else{
        showNotification.error('Bạn Chưa Mua Hàng');
    }
}

const Cart_Order_Page = async () =>{
    const dataSession = sessionStorage.getItem('__CARTS__');
    const txtTotal = document.querySelectorAll('#total-money');
    if(dataSession){
        const value = JSON.parse(dataSession);
        document.getElementById('total-cart').innerText = value.length;
        const money = total_price(value);
         total_money = money.Format_Currency();


        const data = elementItem.cart_order_page(value);
        let html = "";
        for (let index in data) {
          html += data[index];
        }
        document.getElementById("list-cart-order-content").innerHTML = html;
        const AMOUNT =document.querySelectorAll('#chooseAmount');

        for(let clickDOWNUP of AMOUNT){
            const DOWNUP = clickDOWNUP.querySelectorAll('#downup');
            let amountInput =parseInt(clickDOWNUP.getElementsByTagName('input')[0].value);
            for(let click of DOWNUP){
                click.addEventListener('click',(e)=>{
                if(e.target.dataset.value === 'up'){
                    amountInput++;
                }else{
                    if(amountInput != 0){
                        amountInput--; 
                    }
                }
                clickDOWNUP.getElementsByTagName('input')[0].value = amountInput;
                value.map(item =>{
                    if(item.id_item === e.target.dataset.id_item){
                        item.amount = amountInput;
                    }
                })
                total_money = total_price(value).Format_Currency();
                txtTotal[0].innerText =total_money  +' VNĐ';
                txtTotal[1].innerText =total_money  +' VNĐ'; 
                sessionStorage.setItem('__CARTS__',JSON.stringify(value));
            })
            }
        }
    }else{
        document.getElementById("list-cart-order-content").innerHTML = '<h3> Bạn Chưa Mua Sản Phẩm Nào </h3>'
    }
    txtTotal[0].innerText =total_money + ' VNĐ';
    txtTotal[1].innerText =total_money + ' VNĐ'; 
}
const coupon = () =>{

    const inputcoupon = document.getElementById('inputcoupon');
    console.log("pok", inputcoupon.value)

    if(inputcoupon){
        if(inputcoupon.value =='k2store'){
            if(coupon_code !== 0 ){
                showNotification.error('1 Đơn Giảm 1 lần');
            }
            else{
                coupon_code = 100000;
                showNotification.success('COUPON hợp lệ');
                inputcoupon.value ='';
                Cart_Order_Page();
            }
        }

        else {
            showNotification.error('COUPON hợp lệ');
            
        }
       
    }

}
const total_price = (value) =>{
 const price_total = Object.values(value)
    .reduce((previous,key) => {
        let total_price = parseInt(previous);
               return total_price += (parseInt(key.price)*parseInt(key.amount))
    },0);
    return price_total - coupon_code;

};
const DeleteItem = (id_btn) => {
    const id_item  = id_btn.dataset.id_item
    const dataSession = sessionStorage.getItem('__CARTS__');
    const value = JSON.parse(dataSession);
    for(let index in value){
        if(value[index].id_item === id_item){
            value.splice(index,1);
            sessionStorage.setItem('__CARTS__',JSON.stringify(value));
            Cart_Order_Page();
            return;
        }
    }
}