const location_map = [
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1959.7304707214848!2d106.66646677601177!3d10.775965034828019!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752edb9ff98b9f:0xc0b4bddfb6d64358!2zSFVGTElUIE1lZGlhIEdyb3VwL0JhbiBUcnV54buBbiBUaMO0bmcgxJBvw6BuIC0gSOG7mWkgSFVGTElU!5e0!3m2!1svi!2s!4v1593413465926!5m2!1svi!2s",
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.4857735813703!2d106.6736268140202!3d10.774057592323137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752edfa76f6ad5:0xf7aca1c58317762!2zSOG7jWMgdmnhu4duIEjDoG5oIGNow61uaCBRdeG7kWMgZ2lh!5e0!3m2!1svi!2s!4v1593346440425!5m2!1svi!2s",
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.4946681007846!2d106.6584306140202!3d10.773374292323572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ec3c161a3fb%3A0xef77cd47a1cc691e!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBCw6FjaCBraG9hIC0gxJDhuqFpIGjhu41jIFF14buRYyBnaWEgVFAuSENN!5e0!3m2!1svi!2s!4v1593347158411!5m2!1svi!2s",
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.550744231505!2d106.65540851402011!3d10.769065392326517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ec1f0ae533b%3A0x2eeaf642be1cbf1!2zTmjDoCB0aGkgxJHhuqV1IFBow7ogVGjhu40!5e0!3m2!1svi!2s!4v1593347179909!5m2!1svi!2s",
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.298252889509!2d106.59792511425648!3d10.788453461922959!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752c73e8faeeeb%3A0xeded47a4709da934!2sBuffet%20Alibaba%2099K!5e0!3m2!1svi!2s!4v1593523056670!5m2!1svi!2s",
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15677.745679370257!2d106.69566297682209!3d10.777848296479597!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f4649e7ca29%3A0x55f531b6120cae3b!2zTmjDoCBow6F0IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1593523009516!5m2!1svi!2s",
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5907.528239161322!2d106.71631332257806!3d10.727295285312648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f894320f91b%3A0xb6c098e537ce0abe!2sCrescent%20Mall!5e0!3m2!1svi!2s!4v1593522902336!5m2!1svi!2s",
];
const name_map = document.querySelector('#list-location');
for(let index in name_map.children){
    name_map.children[index].addEventListener('click',()=>{
        return document.getElementById('map').setAttribute('src',location_map[index]);
    })
}

                    